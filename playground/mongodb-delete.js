// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err){
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('TodoApp');

    // db.collection('Todos').deleteMany({text: 'Eat Lunch'}).then((result) => {
    //     console.log(result);
    // });

    // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
    //     console.log(result);
    // });

    // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
    //     console.log(result);
    // });

    // db.collection('Users').deleteMany({name: 'Pallavi'}).then((result) => {
    //     console.log(result);
    // });

    // db.collection('Todos').find({
        //     _id: new ObjectID('5dd61a9ac75b8e0eecbe24ad')
        // }).toArray().then((docs) => {
        //     console.log('Todos');
        //     console.log(JSON.stringify(docs, undefined, 2));
        // }, (err) => {
        //     console.log('Unable to fetch todos', err);
        // });

    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5dd65d80c75b8e0eecbe2a75')
    }).then((result) => {
        console.log(JSON.stringify(result ,undefined, 2));
    });
    

    

    // client.close();
});
