// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
    if (err){
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('TodoApp');

    // db.collection('Todos').findOneAndUpdate({
    //     _id: new ObjectID('5dd65b5bc75b8e0eecbe29c4')
    // }, {
    //     $set: {
    //         completed: true
    //     }
    // }, {
    //     returnOriginal: false
    // }).then((result) => {
    //     console.log(result)
    // });

    db.collection('Users').findOneAndUpdate({
        _id: new ObjectID('5dd61a2312a0d32084107164')
    }, {
        $set: {
            name: 'Pallavi Mirje'
        },
        $inc: {
            age: -3
        }
    }, {
        returnOriginal: false
    }).then((result) => {
        console.log(result);
    });


    

    // client.close();
});
