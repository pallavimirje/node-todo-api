const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {Users} = require('./../server/models/users');

// Todo.remove({}).then((result) => {
//    console.log(result) 
// });

Todo.findOneAndRemove({_id: '5dea151bc75b8e0eecbe7dda'}).then((todo) => {
    console.log(todo);
})

// Todo.findByIdAndRemove('5dea1490c75b8e0eecbe7d9f').then((todo) => {
//     console.log(todo);
// });