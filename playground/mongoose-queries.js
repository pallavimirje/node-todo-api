const {ObjectID} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {Users} = require('./../server/models/users');

var id = '5de0ba947c0a2924a4e8d20d';

if (!ObjectID.isValid(id)) {
    console.log("id not valid");
}

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos);
// });

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todo', todo);
// });

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//         return console.log('Id not found');
//     }
//     console.log('Todo By Id', todo);
// }).catch((e) => console.log(e));

Users.findById('5ddba5f6ee65454a78bcfb12').then((users) => {
    if(!users) {
        return console.log('Unable to find user');
    }

    console.log(JSON.stringify(users, undefined, 2));
}, (e) => {
    console.log(e);  
});
